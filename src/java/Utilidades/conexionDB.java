/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ANDRESFELIPE
 */
public class conexionDB {
    
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "123456";
    private static final String HOST = "localhost";
    private static final String PORT = "5432";
    private static final String DATABASE = "Categorias";
    private static final String CLASSNAME = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://" + HOST + ":" + PORT + "/" + DATABASE;

    private java.sql.Connection conexion;

    public conexionDB() {
        try {
            Class.forName(CLASSNAME);
            conexion = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(conexionDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(conexionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cerrar(){
        try {
            conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ResultSet query(String sql) {
        try {
            Statement sentencia = this.conexion.createStatement();
            ResultSet set = sentencia.executeQuery(sql);
            return set;
        } catch (SQLException ex) {
            Logger.getLogger(conexionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int update(String sql) {
        int respuesta;
        try {
            
            Statement sentencia = this.conexion.createStatement();
            respuesta = sentencia.executeUpdate(sql);
            
        } catch (SQLException ex) {
            respuesta = 0;
            Logger.getLogger(conexionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return respuesta;
    }
    /**
     * obtiene la conexion
     * @return 
     */
    public Connection getConexion() {
        return conexion;
    }
    
    
    
    
    
}
