/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Utilidades.conexionDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author ANDRESFELIPE
 */
public class Categoria {
    
    public JSONObject actualizar(JSONObject params){
        
        JSONObject response = new JSONObject();
        conexionDB conexion = new conexionDB();
        String sql = "UPDATE categoria SET descripcion = '" +params.getString("descripcion")+ "' WHERE id_categoria = " +params.getInt("id")+ ";";
        int respuesta = conexion.update(sql);
        conexion.cerrar();
        
        if (respuesta != 0) {
            response.put("ok", "ok");
        }else{
            response.put("ok", "no");
        }
        
        return  response;
        
    }

    public JSONObject agregar(JSONObject params) {
        
        JSONObject response = new JSONObject();
        conexionDB conexion = new conexionDB();
        String sql = "INSERT INTO categoria (descripcion) VALUES ('" + params.getString("descripcion") + "');";
        int respuesta = conexion.update(sql);
        System.out.println(respuesta);
        conexion.cerrar();
        
        if (respuesta != 0) {
            response.put("ok", "ok");
        }else{
            response.put("ok", "no");
        }
        
        return  response;

    }
    
    public JSONObject eliminar(JSONObject params){
        
        JSONObject response = new JSONObject();
        conexionDB conexion = new conexionDB();
        String sql = "DELETE FROM categoria WHERE id_categoria=" + params.getInt("id") + ";";
        int respuesta = conexion.update(sql);
        conexion.cerrar();
        if (respuesta != 0) {
            response.put("ok", "ok");
        }else{
            response.put("ok", "no");
        }
        return response;
    }

    public JSONObject select(JSONObject params) {
        
        JSONObject temp = new JSONObject();
        try {
            
            conexionDB conexion = new conexionDB();
            String sql = "SELECT id_categoria, descripcion FROM categoria";
            ResultSet set = conexion.query(sql);
            conexion.cerrar();
            while(set.next()){
                
                JSONObject aux = new JSONObject();
                aux.put("id_categoria",set.getInt("id_categoria"));
                aux.put("descripcion", set.getString("descripcion"));
                temp.put(set.getInt("id_categoria")+"", aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return temp;
    }

}
