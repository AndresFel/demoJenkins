/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    load();
    initDialog();
});

function load() {
    $("#content").children().remove();
    $.post("/Ciclo1/fachada", JSON.stringify({
        clase: 'Categoria',
        oper: 'select',
        data: {}
    }), function (data) {
        $.each(data, function (key, val) {
            $("<tr><td>" + val.id_categoria + "</td><td>" + val.descripcion + "</td>" +
                    "<td>" +
                    "<button data-action='edit' class='btn btn-primary btn-sm product-edit' " +
                    "data-toggle='modal' " +
                    "data-target='#dialogCategoria' " +
                    "data-desc='" + val.descripcion + "' " +
                    "data-id='" + val.id_categoria + "'>" +
                    "<span class='glyphicon glyphicon-pencil'></span>" +
                    "</button>" +
                    "&nbsp;" +
                    "<button class='btn btn-danger btn-sm delete' data-id='" + val.id_categoria + "'>" +
                    "<span class='glyphicon glyphicon-minus'></span>" +
                    "</button>" +
                    "</td>" +
                    "</tr>").appendTo("#content");
        });
        initRemove();
    }, 'json');
}

function create(descripcion) {
    $.post("/Ciclo1/fachada", JSON.stringify({
        clase: 'Categoria',
        oper: 'agregar',
        data: {descripcion: descripcion}
    }), function (data) {
        if (data.ok === "ok"){
            alert("Categoría creada con éxito");
            load();
        }else{
            alert("se Produjo un error vuelva a intentar");
        }
    }, 'json');
}

function initRemove() {
    $(".delete").unbind().click(function () {
        var id = $(this).data("id");
        remove(id);
    });
}

function remove(idCategoria) {
    $.post("/Ciclo1/fachada", JSON.stringify({
        clase: 'Categoria',
        oper: 'eliminar',
        data: {id: idCategoria}
    }), function (data) {
        if (data.ok === "ok"){
            alert("la Categoria con id "+idCategoria+" fué eliminada");
            load();
        }else{
            alert("se Produjo un error vuelva a intentar");
        }
    }, 'json');
}

function update(id, descripcion) {
    $.post("/Ciclo1/fachada", JSON.stringify({
        clase: 'Categoria',
        oper: 'actualizar',
        data: {id: id, descripcion: descripcion}
    }), function (data) {
        if (data.ok === "ok"){
            alert("la Categoria con id "+id+" fué actualizada");
            load();
        }else{
            alert("se Produjo un error vuelva a intentar");
        }
    }, 'json');
}

function initDialog() {
    $("#dialogCategoria").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var action = button.data('action');
        var id = button.data('id');
        var productAction = $("#productAction");
        productAction.unbind();
        var modal = $(this);
        if (action === "add") {
            modal.find('.modal-title').text("Agregas");
            modal.find('#descripcion').val("");
            productAction.click(function () {
                if (validarDescripcion($("#descripcion").val())) {
                    create($("#descripcion").val());
                    $('#dialogCategoria').modal('toggle');
                }else{
                    alert("descripcion Invalida");
                }
            });
        } else {
            modal.find('.modal-title').text("Editar");
            modal.find('#descripcion').val(button.data("desc"));
            productAction.click(function () {
                update(id, $("#descripcion").val());
                $('#dialogCategoria').modal('toggle');
            });
        }
    });
}

function validarDescripcion(descripcion) {
    var regex = /^[aA-zZ][aA-zZ]*/;
    if (!regex.test(descripcion)) {
        return false;
    } else {
        return true;
    }
}

